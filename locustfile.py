from locust import HttpLocust, TaskSet, task

class WebsiteTasks(TaskSet):

    @task
    def index(self):
        self.client.get("/")

class WebsiteUser(HttpLocust):
    task_set = WebsiteTasks
    host = "http://192.168.1.105:7777/"
    min_wait = 0
    max_wait = 0
