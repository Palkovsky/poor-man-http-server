package main

import (
    "fmt"
    "bufio"
    "io/ioutil"
    "net"
    "./httparser"
    "./httbuilder"
    "./caching"
)

var cache caching.Cache = caching.New()

func main() {
    service := ":7777" // == 0.0.0.0:7777
    tcpAddr, _ := net.ResolveTCPAddr("tcp4", service)
    listener, _ := net.ListenTCP("tcp", tcpAddr) // creating TCP listening socket

    fmt.Printf("Listening on %s...\n", service)
    for {
        conn, _ := listener.Accept()
        go handleRequest(conn)
    }
}

func loadResource(path string) ([]byte, int, bool) {
  if entry, hit := cache.Check(path); hit {
    fmt.Println("Cache hit for: ", path)
    return entry.Content, entry.Length, true
  }

  content, err := ioutil.ReadFile(path)
  if err != nil {
    return make([]byte, 0), 0, false
  }

  entry := cache.Insert(path, content)
  return entry.Content, entry.Length, true
}

func handleError(builder *httbuilder.Response, err string){
  builder.Body([]byte("<h1>" + err + "</h1><p>I'm serious web server.</p>"))
  builder.Message(err)
  builder.Lock()
}

func handleRequest(conn net.Conn) {
  defer conn.Close()

  r := bufio.NewReader(conn)
  w := bufio.NewWriter(conn)

  req, err := httparser.Parse(r)
  response := httbuilder.New(w)

  response.Version(req.Version)
  if err != nil { // parsing error, 400 Bad Request in most cases
    handleError(response, err.Error())
  }

  if content, len, ok := loadResource(req.ResourcePath); ok {
    response.Message("200 OK").BodyWithLength(content, len)
  } else {
    handleError(response, "404 Not Found")
  }

  response.
    ContentType(req.Ext).
    Header("Cache-Control", "no-cache, no-store, must-revalidate").
    Header("Pragma", "no-cache").
    Header("Expires", "0").
    BuildAndFlush()
}
