package httparser

import ("bufio"
        "math"
        "fmt"
        "strings"
        "strconv")

type Request struct {
    Method string
    Path string
    ResourcePath string
    Ext string
    Version string
    Headers map[string]string
    Body []byte
}

type RequestError struct {
    code string
    msg string
}

func (e RequestError) Error() string {
    return e.code + " " + e.msg
}

const MAIN_DIRECTORY = "./www/"
const MAX_BODY_SIZE = 4096
const DEFAULT_FILE = "index.html"

//--------- Parse first line of http request
func (req *Request) parseFirstLine(line string) error {
  methods := []string{"GET", "POST"}
  versions := []string{"HTTP/1.0", "HTTP/1.1", "HTTP/2.0"}

  chunks := strings.Split(line, " ")
  if len(chunks) == 2 { //because http version isn't required in request
    chunks = append(chunks, req.Version)
  }

  if len(chunks) == 3 {
    method, path, version := chunks[0], chunks[1], chunks[2]
    if !stringInSlice(method, methods) {
      return RequestError{"405", "Method Not Allowed"}
    }
    if !validatePath(path){
      return RequestError{"404", "Not Found"}
    }
    if !stringInSlice(version, versions){
      return RequestError{"505", "HTTP Version Not Supported"}
    }

    req.Method = method
    req.Path = path
    req.Ext = extractExt(req.Path)
    req.Version = version
    formatPath(req)

    return nil
  }
  return RequestError{"400", "Bad Request"}
}

//--------- Parses line containing header, returns error if not properly formatted
func (req *Request) parseHeader(line string) error{
  chunks := strings.SplitN(line, ": ", 1)
  if len(chunks) == 2 {
    key, value := chunks[0], chunks[1]
    req.Headers[key] = value
    return nil
  } else {
      return RequestError{"400", "Bad Request"}
  }
}

func (req *Request) init() {
  req.Version = "HTTP/1.1"
  req.Headers = make(map[string]string)
  req.Body = make([]byte, 0)
}

//--------- Infers body size based on request params
func (req Request) inferBodySize() int {
  if req.Method == "GET" {
    return 0
  }
  if val, ok := req.Headers["Content-Length"]; ok {
    if size, err := strconv.Atoi(val); err == nil {
      return int(math.Min(MAX_BODY_SIZE, float64(size)))
    }
  }
  return 0
}

//--------- Main request parsing fuction
func Parse(reader *bufio.Reader) (Request, error){
  var req Request


  line, _, readErr := reader.ReadLine()
  if readErr != nil {
    return req, badRequest()
  }
  fmt.Println(string(line))
  err := req.parseFirstLine(string(line))   // Parsing first line of request
  if err != nil {
    return req, err
  }


  // Parsing headers up to \r\n\r\n or EOF
  for {
    line, _, readErr := reader.ReadLine()
    fmt.Println(string(line))
    if readErr != nil || string(line) == ""{
      break
    }
    parseErr := req.parseHeader(string(line))
    if parseErr != nil {
      break//return req, parseErr
    }
  }

  // Try to parse body
  size := req.inferBodySize()
  if size > 0 {
    buffer := make([]byte, size)
    reader.Read(buffer)
    req.Body = buffer
  }

  return req, nil
}

func badRequest() RequestError {
  return RequestError{"400", "Bad Request"}
}

//------------- Some validation helper functions

func stringInSlice(str string, list []string) bool { //copied from so
 for _, v := range list {
   if v == str {
     return true
   }
 }
 return false
}

func formatPath(req *Request) {
  path := (*req).Path

  if req.Ext == "" && !strings.HasSuffix(path, "/"){
    path = path + "/"
  }

  if strings.HasSuffix(path, "/"){
    path = path + DEFAULT_FILE
  }

  if strings.HasSuffix(MAIN_DIRECTORY, "/"){
    path = MAIN_DIRECTORY + strings.TrimPrefix(path, "/")
  }else {
    path = MAIN_DIRECTORY + path
  }

  (*req).ResourcePath = path
  (*req).Ext = extractExt((*req).ResourcePath)
}

func validatePath(path string) bool {
  // smart browsers take care of this, but still It needs to be validated here
  return strings.HasPrefix(path, "/") && !strings.Contains(path, "..") && !strings.Contains(path, "~")
}

func extractExt(path string) string{
  chunks := strings.Split(path, ".")
  maybeExt := chunks[len(chunks)-1]
  if len(maybeExt) >= 2 && len(maybeExt) <= 4 && !strings.ContainsAny(maybeExt, ".,/\\"){
    return maybeExt
  }
  return ""
}
