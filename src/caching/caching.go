package caching

import ("time"
        "../github.com/orcaman/concurrent-map")

type CacheEntry struct {
  Content []byte
  Length int
  InvalidationTime time.Time
}

// needs to be raplaces with thread-safe map
type Cache struct {
  Store cmap.ConcurrentMap
}

const CACHE_INVALIDATION_TIME = 10 //[s]

func New() Cache {
  var newCache Cache
  newCache.Store = cmap.New()
  return newCache
}

func (cache Cache) Check(key string) (CacheEntry, bool) {
  var entry CacheEntry
  if val, ok := cache.Store.Get(key); ok {

    result := val.(CacheEntry)

    if time.Now().After(result.InvalidationTime) {
      cache.Store.Remove(key)
      return entry, false
    } else {
      return result, true
    }

  }
  return entry, false
}

func (cache Cache) Insert(key string, content []byte) CacheEntry {
  var entry CacheEntry
  entry.Content = content
  entry.Length = len(content)
  entry.InvalidationTime = time.Now().Add(time.Second * CACHE_INVALIDATION_TIME)
  cache.Store.Set(key, entry)
  return entry
}
