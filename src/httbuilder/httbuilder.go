package httbuilder

import ("bufio"
        "fmt"
        "strconv")

type Response struct {
    version string // ex. HTTP/1.1
    message string // ex. 200 OK, 404 Not Found etc.
    body []byte
    headers map[string]string
    locked bool
    writer *bufio.Writer
}

func New(w *bufio.Writer) *Response {
  var builder Response
  builder.Reset()
  builder.writer = w
  return &builder
}

func (res *Response) Build() {
    fmt.Println(res.message)
    res.writer.Write([]byte(res.version + " " + res.message + "\r\n"))
    for k, v := range res.headers {
      res.writer.Write([]byte(k + ": " + v + "\r\n"))
    }
    res.writer.Write([]byte("\r\n"))
    res.writer.Write(res.body)
    res.Reset()
}

func (res *Response) Reset() {
    res.version = ""
    res.message = ""
    res.headers = make(map[string]string)
    res.body = make([]byte, 0)
    res.locked = false
}

func (res *Response) Flush() {
  res.writer.Flush()
}

func (res *Response) BuildAndFlush() {
  res.Build()
  res.Flush()
}

func (res *Response) Version(version string) *Response {
  if !res.IsLocked() {
    res.version = version
  }
  return res
}

func (res *Response) Message(msg string) *Response {
  if !res.IsLocked() {
    res.message = msg
  }
  return res
}

func (res *Response) Header(key string, val string) *Response{
  if !res.IsLocked() {
    res.headers[key] = val
  }
  return res
}

// placeholder for real implementation
func (res *Response) ContentType(ext string) *Response {
  if ext == "" {
    ext = "plain"
  }
  return res.Header("Content-Type", "text/" + ext)
}

func (res *Response) BodyWithLength(content []byte, len int) *Response{
  if !res.IsLocked() {
    res.Header("Content-Length", strconv.Itoa(len))
    res.body = content
  }
  return res
}

func (res *Response) Body(content []byte) *Response{
  return res.BodyWithLength(content, len(content))
}

func (res *Response) Lock() *Response {
  res.locked = true
  return res
}

func (res *Response) Unlock() *Response {
  res.locked = false
  return res
}

func (res *Response) IsLocked() bool {
  return res.locked
}
