package main

import (
    "fmt"
    "bufio"
    "net"
    "os"
    "time"
)

func main() {
    if len(os.Args) != 2 {
        fmt.Fprintf(os.Stderr, "Usage: %s host:port ", os.Args[0])
        os.Exit(1)
    }
    service := os.Args[1]
    tcpAddr, _ := net.ResolveTCPAddr("tcp4", service)

    start := time.Now()
    conn, _ := net.DialTCP("tcp", nil, tcpAddr)

    r := bufio.NewReader(conn)
    w := bufio.NewWriter(conn)

    w.WriteString("GET / HTTP/1.1\r\n\r\n")
    w.Flush()
    result, _ := r.ReadString('\n');
    fmt.Println(string(result))
    fmt.Printf("Responded in %s", time.Since(start))
    os.Exit(0)
}

func checkError(err error) {
    if err != nil {
        fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
        os.Exit(1)
    }
}
